'''
Created on Nov 15, 2015

@author: gkalmanovich
'''

import numpy as np

class red_blue_data:
    def __init__(self, dimm = 5):
        self.ratio = 3.0 #mean over st.dev scale component
        # np.random.seed( 1 )
        self.dimm = dimm
        self.multiplier = self.ratio / np.sqrt(dimm)
        
    def getDataPoint(self):
        color = np.random.choice(2)
        location = np.random.normal(0.0,1.0,self.dimm)
        location += color*self.multiplier
        for i in range(1,self.dimm):
            location[i] += self.multiplier * np.sin(location[i-1])
        return (color,location) # a tuple of (y,x)
        # NOTE, y is red when it is 0 and blue when it is white

def show_test_points(n):
    data = red_blue_data()
    for _ in range(n):
        (y,x) = data.getDataPoint()
        print(""+str(y)+"   "+str(x))

# test_plot()
# print compute_sqr_error_on_data_points()
print("Get one point")
show_test_points(1)
print("Get seven points")
show_test_points(7)

